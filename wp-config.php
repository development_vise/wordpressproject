<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'S87O~,$U6oH?tp<{eHk`OjnNxX7LScvB^q}%iFq*/wx;d( xp%2aYK@R&{hiKo(f');
define('SECURE_AUTH_KEY',  'Nd[|jhU2V:W<Et<;|;[ht8S9r5Rvax|l5knE5%Y=08>CMB$wzz-gkY1@TgvPwd_*');
define('LOGGED_IN_KEY',    '{wbabar@w[sg{|dGa}VMmV{h@p&lJ^$]Ruh!8]HT_#vbNI=8$g$GlQd @MqYKG{#');
define('NONCE_KEY',        'lEhp4SHI)8GoW9bR7Y?~3gxW:2%S!aNQ+*|w0r~CF4x}(s4%r&DS*S$s3DT#nVlw');
define('AUTH_SALT',        'I2#3*M8K2txu]1[5zg%A -Ma6K0T Vh`$aVhWn3N4gG/ r6%vr-m;adv>oitp=,$');
define('SECURE_AUTH_SALT', 'Yj9vVKyW%Om64SFFFkURAi^U%o%zc.^F@*$tRstA<9WNfd+TmAdovyAObs>kb1{X');
define('LOGGED_IN_SALT',   '}<3z4n(|~rX$&1*jq^d]*M^yU]<dD:*{5}B-.V5t.y>^bvZD@WU Rf/jo[PGa1;x');
define('NONCE_SALT',       'v%QPU7k{4V=Z}Mq-BSU9_P0<KZs0):V]*inued[X!l6P2SIWg3*Vva2@Q6fka*+K');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
